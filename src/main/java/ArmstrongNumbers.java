import java.util.Arrays;

public class ArmstrongNumbers {

    public static boolean isArmstrongNumber(int number) {

        char[] digits = String.valueOf(number).toCharArray();

        int sum = 0;
        for (char digit : digits) {
            sum += Math.pow(Character.getNumericValue(digit), digits.length);
        }

        return sum == number;
    }

    //żeby wrzucić na bitbucketa to trzeba:
    //w folderze otworzyc konsole i zrobic git init
    //git add .
    //git commit -m "first commit"
    //utworzyc repo na bitbucket (najlepiej bez readme)
    // git remote add origin https://rafal_kurman@bitbucket.org/rafal_kurman/armstrong2.git
    //git push -u origin master
    //pozniej mozna juz w ide klikac komity i pushe

}
